/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import utilitytypes.EnumOpcode;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import utilitytypes.Logger;
import utilitytypes.Operand;

/**
 * Some other students had the idea to store the Latch object containing a 
 * dispatched instruction into the IQ.  This allowed them to use the pre-
 * existing doForwardingSearch() method to scan for completing inputs for
 * instructions.  I consider that to be an excellent alternative approach
 * to what I did here.
 * 
 * @author millerti
 */
public class IssueQueue extends PipelineStageBase {
    
    public IssueQueue(IModule parent) {
        super(parent, "IssueQueue");
    }
    
    
    // Data structures...
    
    static private class Completion {
        String regname;
        int value;
        boolean next, is_float;
        Completion(String rname, int val, boolean n, boolean f) {
            regname = rname;
            value = val;
            next = n;
            is_float = f;
        }
        public String valueToString() {
            return is_float ? Float.toString(Float.intBitsToFloat(value)) : Integer.toString(value);
        }
    }
    
    static private class IQEntry {
        InstructionBase ins;
        Operand[] operands;
        int output_port;
        IQEntry(InstructionBase ins, Operand[] ops, int outport) {
            this.ins = ins;
            operands = ops;
            output_port = outport;
        }
    }
    
    private int opcodeOutputPort(EnumOpcode op) {
        int output_num;
        if (op == EnumOpcode.FADD || op == EnumOpcode.FSUB || op == EnumOpcode.FCMP) {
            output_num = lookupOutput("IQToFloatAddSub");
        } else
        if (op == EnumOpcode.FDIV) {
            output_num = lookupOutput("IQToFloatDiv");
        } else
        if (op == EnumOpcode.FMUL) {
            output_num = lookupOutput("IQToFloatMul");
        } else
        if (op == EnumOpcode.DIV || op == EnumOpcode.MOD) {
            output_num = lookupOutput("IQToIntDiv");
        } else
        if (op == EnumOpcode.MUL) {
            output_num = lookupOutput("IQToIntMul");
        } else
        if (op.isBranch()) {
            output_num = lookupOutput("IQToBRU");
        } else {
            output_num = lookupOutput("IQToExecute");
        }
     
         return output_num;
    }
    
    int used_entries = 0;
    int use_next_slot = 0;
    IQEntry iq[] = new IQEntry[256];
    Map<Integer,Completion> compl_prev = new HashMap<>();
    Map<Integer,Completion> compl_now  = null;
    Map<Integer,Completion> compl_next = null;
    
    @Override
    public void compute() {
        Map<Integer,String> doing = new HashMap<>();
//        List<String> doing = new ArrayList<String>();
        ICpuCore core = getCore();

        Latch in = readInput(0);
        InstructionBase ins = in.getInstruction();
        IGlobals globals = (GlobalData)getCore().getGlobals();
        
         if( globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==2 || 
                   globals.getPropertyInteger(IProperties.CPU_RUN_STATE)==3)
            {
                in.consume();
                if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==2 )
                {
                    in.setInvalid();
                }
                if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==3)
                {
                    for (int i=0; i<256; i++)
                    {
                        iq[i]=null;
                    }
                }
                
            }
        
        if (!ins.isNull()) {
            int ient = -1;
            for (int i=0; i<256; i++) {
                if (iq[use_next_slot] == null) {
                    ient = use_next_slot;
                    break;
                }
                use_next_slot++;
                use_next_slot &= 255;
            }
            if (ient>=0) {
                used_entries++;
                core.incDispatched();
                doing.put(ient, ins.toString() + " [new]");

                Operand[] ops = {ins.getOper0(), ins.getSrc1(), ins.getSrc2()};
                if (!ins.getOpcode().oper0IsSource()) ops[0] = null;
                for (int i=0; i<3; i++) {
                    if (ops[i] == null) continue;
                    if (!ops[i].isRegister()) ops[i] = null;
                }
                int outport = opcodeOutputPort(ins.getOpcode());
                iq[ient] = new IQEntry(in.getInstruction(), ops, outport);
                in.consume();
            } else {
                addStatusWord("IQ Full");
            }
        }
        
        
//        Logger.out.println("IQ has " + used_entries + " used entries");
        Set<String> fwdsrcs = core.getForwardingSources();
        
        // Loop over the forwarding sources, capturing all values available
        // right now
        compl_now  = new HashMap<>();
        compl_next = new HashMap<>();
        for (String fsrc : fwdsrcs) {
            IPipeReg pipereg = core.getPipeReg(fsrc);
            
            Latch slave = pipereg.read();
            if (!slave.isNull() && slave.hasResultValue()) {
                int fwd_regnum = slave.getResultRegNum();
                if (fwd_regnum >= 0) {
                    int value = slave.getResultValue();
                    boolean isfloat = slave.isResultFloat();
                    compl_now.put(fwd_regnum, new Completion(fsrc, value, false, isfloat));
                }
            }
            
            Latch next = pipereg.readNextCycle();
            if (!next.isNull() && next.hasResultValue()) {
                int fwd_regnum = next.getResultRegNum();
                if (fwd_regnum >= 0) {
                    compl_next.put(fwd_regnum, new Completion(fsrc, 0, true, false));
                }
            }
        }
        
//        if (compl_now.size() > 0) {
//            Logger.out.print("Available now :");
//            for (Map.Entry<Integer,Completion> ent : compl_now.entrySet()) {
//                Logger.out.print(" " + ent.getValue().regname + ":P" + ent.getKey() + "=" + ent.getValue().valueToString());
//            }
//            Logger.out.println();
//        }
//        if (compl_now.size() > 0) {
//            Logger.out.print("Available next:");
//            for (Map.Entry<Integer,Completion> ent : compl_next.entrySet()) {
//                Logger.out.print(" " + ent.getValue().regname + ":P" + ent.getKey());
//            }
//            Logger.out.println();
//        }
        
        // Loop over all IQ entries, storing matching reg values already
        // available
        int real_count = 0;
        for (int i=0; i<256; i++) {
            IQEntry ie = iq[i];
            if (ie == null) continue;
            real_count++;
            
//            Logger.out.print("IQ Entry " + i + " needs:");
            Operand[] operands = ie.operands;
            for (int j=0; j<3; j++) {
                Operand op = operands[j];
                if (op != null && op.isRegister() && !op.hasValue()) {
                    int regnum = op.getRegisterNumber();
                    Completion match1 = compl_now.get(regnum);
                    Completion match2 = compl_prev.get(regnum);
//                    Logger.out.print(" op" + j + "/P" + regnum);
                    if (match1 != null && match2 != null) {
                        throw new RuntimeException("Two completions to the same physreg on consecutive cycles");
                    }
                    if (match1 != null) {
                        op.setValue(match1.value, match1.is_float);
                        Logger.out.println("# Forward from " + match1.regname + " this cycle to IQ: op" + j + " of " + ie.ins);
                    }
                    if (match2 != null) {
                        op.setValue(match2.value, match2.is_float);
                        Logger.out.println("# Forward from " + match1.regname + " prev cycle to IQ: op" + j + " of " + ie.ins);
                    }
                }
            }
        }
        
        // Save updates this cycle for instructions in transition from 
        // Decode to IQ on next cycle.
        compl_prev = compl_now;
        
        
        // Find out which output ports can accept new work
        int numout = numOutputRegisters();
        boolean[] canAcceptWork = new boolean[numout];
        for (int i=0; i<numout; i++) {
            canAcceptWork[i] = outputCanAcceptWork(i);
        }
        
        // Perform selection on instructions in IQ
        Latch[] selected = new Latch[numout];
        int[] selected_index = new int[numout];
        String[] fwd_src = new String[3];
        for (int i=0; i<256; i++) {
            IQEntry ie = iq[i];
            if (ie == null) continue;
            int port = ie.output_port;
            if (!canAcceptWork[port] || selected[port] != null) {
                if (!doing.containsKey(i)) {
                    doing.put(i, ie.ins.toString());
                }
                continue;
            }
            
//            Logger.out.println("Slot " + i + ": " + ie.ins);
            
            Operand[] operands = ie.operands;
            boolean fail = false;
            for (int j=0; j<3; j++) {
                fwd_src[j] = null;
                Operand op = operands[j];
                if (op != null && !op.hasValue()) {
                    int regnum = op.getRegisterNumber();
//                    Logger.out.println("op" + j + " matching reg" + regnum);
                    Completion match = compl_next.get(regnum);
                    if (match == null) {
                        fail = true;
                        break;
                    } else {
                        fwd_src[j] = match.regname;
                    }
                }
            }
            if (fail) {
                if (!doing.containsKey(i)) {
                    doing.put(i, ie.ins.toString());
                }
                continue;
            }
            
            Latch out = this.newOutput(port);
            String next_stage_name = out.getParentRegister().getStageAfter().getName();
//            Logger.out.println("# IQ issuing from slot " + i + " to " + next_stage_name + ": " + ie.ins);
            if (doing.containsKey(i)) {
                doing.put(i, doing.get(i) + " [selected]");
            } else {
                doing.put(i, ie.ins.toString() + " [selected]");
            }
            out.setInstruction(ie.ins);
            for (int j=0; j<3; j++) {
                if (fwd_src[j] != null) {
                    out.setProperty("forward"+j, fwd_src[j]);
                    Logger.out.println("# Posting forward from " + fwd_src[j] + " next cycle to " + next_stage_name + ": op" + j + " of " + ie.ins);
                }
            }
            selected[port] = out;
            selected_index[port] = i;
        }
        
        for (int i=0; i<numout; i++) {
            if (selected[i] != null) {
                used_entries--;
                core.incIssued();
                selected[i].write();
                iq[selected_index[i]] = null;
            }
        }
        
        setActivity(String.join("\n", doing.values()));
    }

    
}
