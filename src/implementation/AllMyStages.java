/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import tools.MyALU;
import utilitytypes.EnumOpcode;
import baseclasses.InstructionBase;
import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import voidtypes.VoidLatch;
import baseclasses.CpuCore;
import baseclasses.Latch;
import cpusimulator.CpuSimulator;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import utilitytypes.ClockedIntArray;
import static utilitytypes.EnumOpcode.*;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;
import voidtypes.VoidLabelTarget;

/**
 * The AllMyStages class merely collects together all of the pipeline stage 
 * classes into one place.  You are free to split them out into top-level
 * classes.
 * 
 * Each inner class here implements the logic for a pipeline stage.
 * 
 * It is recommended that the compute methods be idempotent.  This means
 * that if compute is called multiple times in a clock cycle, it should
 * compute the same output for the same input.
 * 
 * How might we make updating the program counter idempotent?
 * 
 * @author
 */
public class AllMyStages {
    /*** Fetch Stage ***/
    static class Fetch extends PipelineStageBase {
        public Fetch(ICpuCore core) {
            super(core, "Fetch");
        }
        
        // Does this state have an instruction it wants to send to the next
        // stage?  Note that this is computed only for display and debugging
        // purposes.
        boolean has_work;
        boolean shutting_down = false;        
        /** 
         * For Fetch, this method only has diagnostic value.  However, 
         * stageHasWorkToDo is very important for other stages.
         * 
         * @return Status of Fetch, indicating that it has fetched an 
         *         instruction that needs to be sent to Decode.
         */
        @Override
        public boolean stageHasWorkToDo() {
            return has_work;
        }
        
        @Override
        public String getStatus() {
            IGlobals globals = (GlobalData)getCore().getGlobals();
            if (globals.getPropertyInteger("branch_state_fetch") == GlobalData.BRANCH_STATE_WAITING) {
                addStatusWord("ResolveWait");
            }
            return super.getStatus();
        }

        @Override
        public void compute(Latch input, Latch output) {
            
            if (shutting_down) {
                addStatusWord("Shutting down");
                setActivity("");
                return;
            }
            
            IGlobals globals = (GlobalData)getCore().getGlobals();
            
            // Get the PC and fetch the instruction
            int pc    = globals.getPropertyInteger(PROGRAM_COUNTER);
            int branch_state_fetch = globals.getPropertyInteger("branch_state_fetch");
            
            
            boolean branch_wait = false;
            
            //PC from recovery logic
            if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) == 4)          // state chage to recovery
            {
                globals.setClockedProperty("branch_state_fetch", GlobalData.BRANCH_STATE_NULL);  
                pc=globals.getPropertyInteger(IProperties.RECOVERY_PC);
                branch_wait = false;
            }
            
           
            
            InstructionBase ins = globals.getInstructionAt(pc);
            
            // Initialize this status flag to assume a stall or bubble condition
            // by default.
            has_work = false; 
           
            if (ins.isNull()) {
                // Fetch is working on no instruction at no address
                setActivity("");
            } else {            
                // Since there is no input pipeline register, we have to inform
                // the diagnostic helper code explicitly what instruction Fetch
                // is working on.
                has_work = true;
                output.setInstruction(ins);
                setActivity(ins.toString());
            }
           
            
            // If the output cannot accept work, then 
            if (!output.canAcceptWork()) return;


            if (ins.getOpcode() == EnumOpcode.HALT) shutting_down = true;

            globals.setClockedProperty(PROGRAM_COUNTER, pc + 1);
                     
            if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) == 2)          // state change to fault
            {
                globals.setClockedProperty("branch_state_fetch", GlobalData.BRANCH_STATE_WAITING);
                branch_wait = true;
            }
           
            if (!branch_wait) {
                if (ins.getOpcode().isBranch()) {
                    
                   if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) == 4)          // state chage to recovery
                   {
                        if(globals.getPropertyBoolean(IProperties.RECOVERY_TAKEN) ) {
                            globals.setClockedProperty(PROGRAM_COUNTER, ins.getLabelTarget().getAddress());
                        }
                        globals.setClockedProperty(IProperties.CPU_RUN_STATE,1);
                        
                   }
                   else{
                             if(ins.getOpcode()==EnumOpcode.JMP || ins.getOpcode()==EnumOpcode.CALL){
                                ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);
                                 globals.setClockedProperty(PROGRAM_COUNTER, ins.getLabelTarget().getAddress());
                             }
                             if(ins.getOpcode()==EnumOpcode.BRA){
                                if(ins.getPCAddress()>ins.getLabelTarget().getAddress()){
                                    ins.setBranchPrediction(InstructionBase.EnumBranch.TAKEN);
                                    globals.setClockedProperty(PROGRAM_COUNTER, ins.getLabelTarget().getAddress());
                                }
                                else
                                    ins.setBranchPrediction(InstructionBase.EnumBranch.NOT_TAKEN);
                             }
                   }
                }
            }
        }
    }

    /*** Decode Stage ***/
    static class Decode extends PipelineStageBase {
        public Decode(ICpuCore core) {
            super(core, "Decode");
        }
        
        
        // When a branch is taken, we have to squash the next instruction
        // sent in by Fetch, because it is the fall-through that we don't
        // want to execute.  This flag is set only for status reporting purposes.
        boolean squashing_instruction = false;
        boolean shutting_down = false;

        @Override
        public String getStatus() {
            IGlobals globals = (GlobalData)getCore().getGlobals();
            String s = super.getStatus();
            if (globals.getPropertyBoolean("decode_squash")) {
                s = "Squashing";
            }
            return s;
        }
        

//        private static final String[] fwd_regs = {"ExecuteToWriteback", 
//            "MemoryToWriteback"};
        
        @Override
        public void compute() {
            if (shutting_down) {
                addStatusWord("Shutting down");
                setActivity("");
                return;
            }
            Latch input = readInput(0);
            input = input.duplicate();
            InstructionBase ins = input.getInstruction();

            // Default to no squashing.
            //squashing_instruction = false;
            
            setActivity(ins.toString());
            
            if (ins.isNull()) return;
            
            IGlobals globals = (GlobalData)getCore().getGlobals();
          
            if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE)==2 ||
                   globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==5 ||
                   globals.getPropertyInteger(IProperties.CPU_RUN_STATE)==6)
            {
                input.consume();
                return;
            }
            
            int rob_head = globals.getPropertyInteger(ROB_HEAD);
            int rob_tail = globals.getPropertyInteger(ROB_TAIL);
            boolean rob_full = ((rob_tail + 1) & 255) == rob_head;
            
            
            if(rob_full){
                
                setResourceWait("ROB is Full");
                return;
                
            }
            
            ins.setReorderBufferIndex(rob_tail);
            
            
            Latch output;
            int output_num;
            
            if(ins.getOpcode().accessesMemory())
            {
                // LSQ
                output_num = lookupOutput("DecodeToLSQ");
                output = this.newOutput(output_num);
                
            }
            else
            {
                // IQ
                output_num = lookupOutput("DecodeToIQ");
                output = this.newOutput(output_num);
            }
            
            if(ins.getOpcode()==EnumOpcode.CALL)
            {
                Operand pc_operand = Operand.newRegister(Operand.PC_REGNUM);
                pc_operand.setIntValue(ins.getPCAddress());
                ins.setSrc1(pc_operand);
                ins.setSrc2(Operand.newLiteralSource(1));
            }
            
            EnumOpcode opcode = ins.getOpcode();
            Operand oper0 = ins.getOper0();
            Operand src1  = ins.getSrc1();
            Operand src2  = ins.getSrc2();
            IRegFile regfile = globals.getRegisterFile();
            int[] rat = globals.getPropertyIntArray("rat");
            
            // Rename sources
            if (opcode.oper0IsSource() && oper0.isRegister()) {
                oper0.rename(rat[oper0.getRegisterNumber()]);
            }
            if (src1.isRegister()) {
                src1.rename(rat[src1.getRegisterNumber()]);
            }
            if (src2.isRegister()) {
                src2.rename(rat[src2.getRegisterNumber()]);
            }
            
            
        
            // See what operands can be fetched from the register file
            registerFileLookup(input);
            // See what operands can be fetched by forwarding
            forwardingSearch(input);
            
            
             if (!output.canAcceptWork()) return;
            
              // Mark the destination register invalid
            if (opcode.needsWriteback()) {
                 int archregnumber=oper0.getRegisterNumber();
                //IRegFile arfregfile = globals.getPropertyRegisterFile(ARCH_REG_FILE);
                ClockedIntArray RAT = globals.getPropertyClockedIntArray(REGISTER_ALIAS_TABLE);
                int oldphyreg=RAT.get(archregnumber);
                if(oldphyreg>0){
                       regfile.markUnmapped(oldphyreg, true);
                }
                regfile.markNewlyAllocated(rob_tail);
            } 
            else{
                regfile.markNewlyAllocated(rob_tail);
            }
            
            
            // Copy the forward# properties
            output.copyAllPropertiesFrom(input);
            // Copy the instruction
            output.setInstruction(ins);
            // Send the latch data to the next stage
            output.write();
            
            //store instruction into ROB indexed by ROB_Tail
            InstructionBase []ROB=globals.getPropertyInstructionArr(REORDER_BUFFER);
            
            ROB[rob_tail]=ins;
            globals.setClockedProperty(REORDER_BUFFER, ROB);
            if(((rob_tail + 1 +1) & 255) == rob_head)
                globals.setProperty(ROB_TAIL, 0);
            else
                globals.setProperty(ROB_TAIL, rob_tail+1);
            
         //  int rob_head = globals.getPropertyInteger(ROB_HEAD);
           // globals.setProperty(ROB_USED, ins//);
           
      
            // And don't forget to indicate that the input was consumed!
            input.consume();
        }
    }
    

    /*** Execute Stage ***/
    static class Execute extends PipelineStageBase {
        public Execute(ICpuCore core) {
            super(core, "Execute");
        }

        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) return;
            doPostedForwarding(input);
            InstructionBase ins = input.getInstruction();

            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();
            int oper0 =   ins.getOper0().getValue();

            int result = MyALU.execute(ins.getOpcode(), source1, source2, oper0);
                        
            boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
            output.setResultValue(result, isfloat);
            output.setInstruction(ins);
        }
    }
    

}
