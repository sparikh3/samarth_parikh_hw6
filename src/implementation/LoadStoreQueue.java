/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import static implementation.GlobalData.LSQ_SIZE;
import static java.lang.System.in;
import static utilitytypes.IProperties.REORDER_BUFFER;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import utilitytypes.EnumOpcode;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeReg;
import utilitytypes.IProperties;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;

/**
 *
 * @author millerti
 */
public class LoadStoreQueue extends PipelineStageBase {
    
    //InstructionBase[] LSQ = new InstructionBase[LSQ_SIZE];
   // boolean[] markedRetired= new boolean[LSQ_SIZE];
    //static int lastIndex=0;
    
    
    static private class Completion 
    {
        String regname;
        int value;
        boolean next, is_float;
        Completion(String rname, int val, boolean n, boolean f) {
            regname = rname;
            value = val;
            next = n;
            is_float = f;
        }
        public String valueToString() {
            return is_float ? Float.toString(Float.intBitsToFloat(value)) : Integer.toString(value);
        }
    }
    
    
     static private class LSQEntry {
        InstructionBase ins;
        Operand[] operands;
        int outputnumber;			// extra field to mark the output latch
        boolean markedRetired;
        LSQEntry(InstructionBase ins, Operand[] ops, int outnumber, boolean markedRetired) 
        {
            this.ins = ins;
            operands = ops;
            outputnumber = outnumber;
            this.markedRetired=markedRetired;
        }
    }
     
     
      private int opcodeOutputPort(EnumOpcode op) 
      {
        int output_num;
        output_num = lookupOutput("LSQToD1Cache");
        return output_num;
      }
    
    
    
    public LoadStoreQueue(IModule parent) {
        super(parent, "LoadStoreQueue");   
        this.disableTwoInputCompute();
    }
    
    
    // Data structures
    int used_entries = 0;
    int use_next_slot = 0;
    LoadStoreQueue.LSQEntry lsq[] = new LoadStoreQueue.LSQEntry[256];
     
    public void compute() {  
        Map<Integer,String> doing = new HashMap<>();
        //        List<String> doing = new ArrayList<String>();
        ICpuCore core = getCore();
        
        Latch in = readInput(0); 
        InstructionBase ins = in.getInstruction();
        IGlobals globals = (GlobalData)getCore().getGlobals();
        InstructionBase []ROB= globals.getPropertyInstructionArr(REORDER_BUFFER);
        IRegFile arfregfile = globals.getPropertyRegisterFile(GlobalData.ARCH_REG_FILE);
        // First, mark any retired STORE instructions
         
        Set<Integer> retiredSet=core.getRetiredSet();
        for(Integer p:retiredSet)
            lsq[p].markedRetired=true;
        
        // Check CPU run state
        if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE)==2 ||
                globals.getPropertyInteger(IProperties.CPU_RUN_STATE)==3 ||  globals.getPropertyInteger(IProperties.CPU_RUN_STATE)==5 ){
            
                
                in.consume();
                
                if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) == 2 )
                {
                    in.setInvalid();
                }
                
                if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==3 || globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==5)
                {
                   
                    for (int i=0; i<256; i++)
                    {
                        if(lsq[i]!=null)
                            if(lsq[i].ins.getOpcode()==EnumOpcode.STORE && !lsq[i].markedRetired)
                                lsq[i]=null;
                           
                    }
                }
                
                boolean retiredStorePresent=false;
                if(globals.getPropertyInteger(IProperties.CPU_RUN_STATE) ==5 ){
                    for (int i=0; i<256; i++)
                    {
                        if(lsq[i]!=null)
                          if(lsq[i].ins.getOpcode()==EnumOpcode.STORE && lsq[i].markedRetired){
                              retiredStorePresent=true;  
                              break;
                          }
                    }
                    if(retiredStorePresent)
                        globals.setProperty(IProperties.CPU_RUN_STATE, 6);
                }
            
            
        }
            
       
        // If the LSQ is full and a memory instruction wants to come in from
        // Decode, optionally see if you can proactively free up a retired STORE.
        
        if (!ins.isNull()) {
            int ient = -1;
            for (int i=0; i<256; i++) {
                if (lsq[use_next_slot] == null) {
                    ient = use_next_slot;
                    break;
                }
                use_next_slot++;
                use_next_slot &= 255;
            }
            if (ient>=0) {
                used_entries++;
                core.incDispatched();
                doing.put(ient, ins.toString() + " [new]");

                Operand[] ops = {ins.getOper0(), ins.getSrc1(), ins.getSrc2()};
                if (!ins.getOpcode().oper0IsSource()) ops[0] = null;
                for (int i=0; i<3; i++) {
                    if (ops[i] == null) continue;
                    if (!ops[i].isRegister()) ops[i] = null;
                }
                int outport = opcodeOutputPort(ins.getOpcode());
                lsq[ient] = new LoadStoreQueue.LSQEntry(in.getInstruction(), ops, outport,false);
                in.consume();
            } else {
                addStatusWord("LSQ Full");
            }
        }
        
        // See if there's room to storea an instruction received from Decode.
        // Read the input latch.  If it's valid, put the instruction into the 
        // LAST slot.
        // LSQ entries are stored in order from oldest to newest.   
        
       
        Map<Integer,LoadStoreQueue.Completion> compl_prev = new HashMap<>();
        Map<Integer,LoadStoreQueue.Completion> compl_now  = null;
        Map<Integer,LoadStoreQueue.Completion> compl_next = null;
    
   
        
        Set<String> fwdsrcs = core.getForwardingSources();
        
            // Loop over the forwarding sources, capturing all values available
            // right now
            compl_now  = new HashMap<>();
            compl_next = new HashMap<>();
            for (String fsrc : fwdsrcs) {
                IPipeReg pipereg = core.getPipeReg(fsrc);

                Latch slave = pipereg.read();
                if (!slave.isNull() && slave.hasResultValue()) {
                    int fwd_regnum = slave.getResultRegNum();
                    if (fwd_regnum >= 0) {
                        int value = slave.getResultValue();
                        boolean isfloat = slave.isResultFloat();
                        compl_now.put(fwd_regnum, new LoadStoreQueue.Completion(fsrc, value, false, isfloat));
                    }
                }

                Latch next = pipereg.readNextCycle();
                if (!next.isNull() && next.hasResultValue()) {
                    int fwd_regnum = next.getResultRegNum();
                    if (fwd_regnum >= 0) {
                        compl_next.put(fwd_regnum, new LoadStoreQueue.Completion(fsrc, 0, true, false));
                    }
                }
        }
            
            
            
        
            
            

        // Loop over the forwarding sources, capturing all values available
        // right now and storing them into LSQ entries needing those
        // register values.
        
        
        
        // Loop over all IQ entries, storing matching reg values already
        // available
        int real_count = 0;
        for (int i=0; i<256; i++) {
            LoadStoreQueue.LSQEntry ie = lsq[i];
            if (ie == null) continue;
            real_count++;
            
//            Logger.out.print("IQ Entry " + i + " needs:");
            Operand[] operands = ie.operands;
            for (int j=0; j<3; j++) {
                Operand op = operands[j];
                if (op != null && op.isRegister() && !op.hasValue()) {
                    int regnum = op.getRegisterNumber();
                    LoadStoreQueue.Completion match1 = compl_now.get(regnum);
                    LoadStoreQueue.Completion match2 = compl_prev.get(regnum);
//                    Logger.out.print(" op" + j + "/P" + regnum);
                    if (match1 != null && match2 != null) {
                        throw new RuntimeException("Two completions to the same physreg on consecutive cycles");
                    }
                    if (match1 != null) {
                        op.setValue(match1.value, match1.is_float);
                        Logger.out.println("# Forward from " + match1.regname + " this cycle to IQ: op" + j + " of " + ie.ins);
                    }
                    if (match2 != null) {
                        op.setValue(match2.value, match2.is_float);
                        Logger.out.println("# Forward from " + match1.regname + " prev cycle to IQ: op" + j + " of " + ie.ins);
                    }
                }
            }
        }
        
        // Save updates this cycle for instructions in transition from 
        // Decode to IQ on next cycle.
        compl_prev = compl_now;
        
        
        
        // For diagnostic purposes, iterate existing entries of the LSQ and add 
        // their current state to the activity list
        
        int output_num = lookupOutput("LSQToDCache1");
        Latch output = this.newOutput(output_num);
        // Next, try to do things that require writing to the
        // next pipeline stage.  If we have already written the output, or
        // the output can't accept work, might as well bail out.
        if ( !outputCanAcceptWork(0)) {
            setActivity(String.join("\n", doing.values()));
            return;
        }
        
        //----------------------- Optional Start ----------------------------------------------------------
        
        // If and only if there is a LOAD in the FIRST entry of the LSQ, it can be
        // issued to the DCache if needed inputs (to compute the address)
        // will be available net cycle.  
        // Set appropriate "forward#" properties on output latch.
        // Only when a LOAD is not prededed by STOREs can be be issued with
        // forwarding in the next cycle.
        // ******
        // When issuing any LOAD, other entries in the LSQ must be shifted
        // to fill the gap.  The LSQ must maintain program order.  This applies
        // to all cases where the LSQ issues a LOAD.
        // ******
        
        // If we issued a load, bail out.  ** Before bailing out, ALWAYS make sure
        // that setActivity has been called with info about all the 
        // instructons in the queue.  This is for diagnostic purposes. **
       
         
        
        // Look for a load whose address matches that of a store earlier in the list.
        // Since we don't do speculative loads, if a store is encountered with an
        // unknown address, then no subsequent loads can be issued.
        
        //---------------------  Optional End ------------------------------------------------------------

        // Outer loop:  Iterate over all LOAD instructions from first to last
        // LSQ entries.
        int loadindice=-1;
        int storeindice=-1;
        int NoMatchStore=0;
        boolean matchFound=false;
        int loadtoIssue=-1;
        for(int i=0;i<256;i++){
            
            if(lsq[i].ins.getOpcode()==EnumOpcode.STORE || !lsq[i].ins.getOper0().hasValue())
            {
                    continue;
            }
            if(lsq[i].ins.getOpcode()==EnumOpcode.LOAD)
            {
                boolean skip_Loadins=false;
                boolean addressMatchedFlag=false;
                int loadadd=lsq[i].ins.getSrc1().getValue()+lsq[i].ins.getSrc2().getValue();
                int j=0;
                for( j=i-1;j>=0;j--){
                    if(lsq[j].ins.getOpcode()==EnumOpcode.LOAD){
                        continue;
                    }
                    if(lsq[j].ins.getOpcode()==EnumOpcode.STORE && !lsq[i].ins.getOper0().hasValue()){
                        skip_Loadins=true;
                        break;
                        
                    }
                    if(lsq[j].ins.getOpcode()==EnumOpcode.STORE && lsq[i].ins.getOper0().hasValue()){
                        int storeadd=lsq[j].ins.getSrc1().getValue()+lsq[j].ins.getSrc2().getValue();
                       if( loadadd == storeadd){
                           addressMatchedFlag=true;  
                           break;
                       }
                    }                
                }
                
                
                if(skip_Loadins)
                    continue;
                
                if(addressMatchedFlag)
                {
                    if(lsq[j].ins.getOper0().hasValue())
                    {
                        loadindice=i;
                        storeindice=j;
                        matchFound=true;
                        loadtoIssue=i;
                    	break;
                    }
                    else
                    	continue;
                }
                else
                {
                	NoMatchStore=1;
                	loadtoIssue=i;
                	break;
                }
            }
        }
        
        if(loadtoIssue!=-1)
        {
        	if(matchFound)
        	{
        		output.setResultValue(lsq[storeindice].ins.getOper0().getValue(), lsq[storeindice].ins.getOper0().isFloat());	
        		output.setProperty("bypassLoad", true);
        	}
        	else
        		output.setProperty("accessLoad", true);
        	
        	 for(int l=loadtoIssue;l<255;l++)
             {
        		 lsq[l]=lsq[l+1];
             }
        	 used_entries--;
        	 setActivity(String.join("\n", doing.values()));
        	 return;
        }
        core.incCompleted();
        for(int i=0;i<256;i++){
        
        	if(lsq[i].ins.getOpcode()==EnumOpcode.LOAD )continue;
        	if(lsq[i].ins.getOpcode().oper0IsSource())
        	{
        		if(lsq[i].ins.getOper0().isRegister())
        		{
        			if(!lsq[i].ins.getOper0().hasValue())
        			{
        				continue;
        			}
        		}
        	}
        	else
        	{
        		if(lsq[i].ins.getSrc1().isRegister())
        		{
        			if(!lsq[i].ins.getSrc1().hasValue())
        			{
        				continue;
        			}
        		}
        		if(lsq[i].ins.getSrc2().isRegister())
        		{
        			if(!lsq[i].ins.getSrc2().hasValue())
        			{
        				continue;
        			}
        		}
        	}
        	if(!arfregfile.isValid(lsq[i].ins.getReorderBufferIndex()))
        	{
        		continue;
        	}
        	if(core.getRetiredSet().contains(lsq[i].ins.getReorderBufferIndex())) 
        	{
        		continue;
        	}
        	output.setInstruction(lsq[i].ins);
        	output.setProperty("IssueStore", true);
        	 setActivity(String.join("\n", doing.values()));
        }
        
        if(core.getRetiredSet().contains(lsq[0].ins.getReorderBufferIndex()))
        	{
        	output.setInstruction(lsq[0].ins);
        	output.setProperty("CommitStore", true);
        	 setActivity(String.join("\n", doing.values()));


        	 for(int l=0;l<255;l++)
             {
        		 lsq[l]=lsq[l+1];
             }
        	}
        
        // Inner loop:  Iterate backwards over STOREs that came before the LOAD.
        // If you find a STORE with a unknown address, skip to the next LOAD
        // in the outer loop.
        
        // If you find a STORE with a matching address, make sure the STORE
        // has a data value. If it does, this LOAD can be ussued as a BYPASS LOAD:
        // copy the value from the STORE to the LOAD and issue the load, 
        // instructing the DCache to NOT fetch from memory.
        // If the STORE does not have a data value, skip to the next LOAD in 
        // the outer loop.  
        // Data that is forwarded from a STORE to a LOAD must some from the
        // matching STORE that is NEAREST to the LOAD in the list.
        
        // If the inner loop finishes and finds neither a matching STORE addresss
        // nor an unknown store address, this LOAD can be issued as an ACCESS
        // LOAD:  The data is fetched from main memory.
                
        // If we issued a LOAD, set activity string, bail out

        // If we find no LOADs to process, see if there are any STORES to ISSUE.
        // An issuable store has known address and data.  To the DCache,
        // an ISSUE STORE passes through to Writeback without modifying memory.  (It will
        // modify memory later on retirement.)  Also, stores that are issued
        // are NOT REMOVED from the LSQ.  Simply mark them as completed.
        
        // If we issued a STORE, set activity string, bail out
        
        // Finally, see if there is a STORE that can be COMMITTED (retired).
        // Only the FIRST entry of the LSQ can be retired (to maintain 
        // program order).
        // To the DCache, a COMMIT STORE writes its data value to memory
        // but is NOT passed on to Writeback.
        
        // Set activity string, return        
        
        // NOTE:
        // ***
        // Whenever you issue any instruction, be sure to call core.incIssued();
        // This is also the case for the IssueQueue.
        // ***
    }
}
