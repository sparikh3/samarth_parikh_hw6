/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import baseclasses.InstructionBase;
import baseclasses.InstructionBase.EnumBranch;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.EnumComparison;
import utilitytypes.EnumOpcode;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IProperties;
import utilitytypes.IRegFile;
import utilitytypes.Operand;
import voidtypes.VoidLabelTarget;

/**
 * @author millerti
 */
public class BranchResUnit extends PipelineStageBase {

    public BranchResUnit(IModule parent) {
        super(parent, "BranchResUnit");
    }
    
    static boolean resolveBranch(EnumComparison condition, int value0) {
        // Add code here...
        boolean take_branch=true;
         switch (condition) {
                        case EQ:
                            take_branch = (value0 == 0);
                            break;
                        case NE:
                            take_branch = (value0 != 0);
                            break;
                        case GT:
                            take_branch = (value0 > 0);
                            break;
                        case GE:
                            take_branch = (value0 >= 0);
                            break;
                        case LT:
                            take_branch = (value0 < 0);
                            break;
                        case LE:
                            take_branch = (value0 <= 0);
                            break;
                    }
         return take_branch;
                   
    }

    @Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) return;
        doPostedForwarding(input);
        InstructionBase ins = input.getInstruction().duplicate();
        IGlobals globals = (GlobalData)getCore().getGlobals();
        /*
        JMP -- pass through
        BRA -- resolve, compare to prediction, set fault on instruction for disagreement
        CALL -- compute return address and pass on as result value.
        */
        
        
        
            EnumOpcode opcode = ins.getOpcode();
            Operand oper0 = ins.getOper0();
            IRegFile regfile = globals.getRegisterFile();
            Operand src1  = ins.getSrc1();
            Operand src2  = ins.getSrc2();
            
            
            boolean take_branch = false;
            int value0 = 0; 
            
            switch (opcode) {
                case BRA:
                    if (!oper0.hasValue()) {
                        // If we do not already have a value for the branch
                        // condition register, must stall.
//                        Logger.out.println("Stall BRA wants oper0 R" + oper0.getRegisterNumber());
                        this.setResourceWait(oper0.getRegisterName());
                        // Nothing else to do.  Bail out.
                        return;
                    }
                    value0 = oper0.getValue();
                    
                    // The CMP instruction just sets its destination to
                    // (src1-src2).  The result of that is in oper0 for the
                    // BRA instruction.  See comment in MyALU.java.
                   
                    
                    take_branch=resolveBranch(ins.getComparison(),value0);
                    if(take_branch)
                        ins.setBranchResolution(EnumBranch.TAKEN);
                    else
                        ins.setBranchResolution(EnumBranch.NOT_TAKEN);
                    
                    if(ins.getBranchPrediction()!=ins.getBranchResolution())
                    {
                        ins.setFault(InstructionBase.EnumFault.BRANCH);
                    }
                    
                    break;
                    
                case JMP:
                 
                    break;
                    
                case CALL:

                    int result = src1.getValue() + src2.getValue();
                    
                    if (!output.canAcceptWork()) return;
                    
                    output.setResultValue(result);
                    
                    break;
            } 
            
             output.setInstruction(ins);
             output.write();
             input.consume();
    }
    
    
}
